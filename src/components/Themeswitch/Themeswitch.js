import React from "react";

export default function Themeswitch (props) {

    const setTheme = () => {
        props.onSetMode(props.targetMode)
    }

    return (
        <div>
            <div onClick={setTheme} className={`${props.mode}-outer-circle`}>
                <div className={`${props.mode}-inner-circle` + (props.mode === props.targetMode ? 'inner-circle-hidden' : '')}>
                </div>
            </div>

            <div>
                <p className="theme-switch-hint">{props.targetMode === "light" ? <p>o n</p> : <p>o f f</p>}</p>
            </div>
        </div>
    )
}
