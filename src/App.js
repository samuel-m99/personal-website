import React, {useState} from "react";

import "./components/styles.css";
import "./components/Portfolio/Portfolio.css";
import "./components/Themeswitch/Themeswitch.css";

import avatar from "./img/avatar.jpg"
import Portfolio from "./components/Portfolio/Portfolio.js"
import Themeswitch from "./components/Themeswitch/Themeswitch.js"

function App() {
  const [mode, setMode] = useState("dark")

  return (
    <div className={`${mode}-mode`}>
    <p className="mail">samuel.mandak99@gmail.com</p>
      <div className="theme-switch">
        <Themeswitch
          targetMode="light"
          mode={mode}
          onSetMode={setMode}
        />
        <Themeswitch
          targetMode="dark"
          mode={mode}
          onSetMode={setMode}
        />
      </div>

      <div className="main">
        <img src={avatar}/>
        <p>
          Hi. I'm <mark className={`highlight-${mode}`}>Samuel Mandák</mark>, an aspiring <mark className={`highlight-${mode}`}>Front-End</mark> Junior Developer from the beautiful city of <mark className={`highlight-${mode}`}>Nitra</mark> in Slovakia. I'm currently studying at the Slovak University of Agriculture, where I'll soon be getting my Master's degree. In 2021, I started learning C# but didn't find it particularly exciting. In late <mark className={`highlight-${mode}`}>2022</mark>, I switched gears and began exploring <mark className={`highlight-${mode}`}>JavaScript</mark>, which has become one of my favorite languages to work with. Throughout my journey, there have been pauses as life happens. Alongside JavaScript, I've also learned <mark className={`highlight-${mode}`}>React, CSS, and HTML</mark> properly. In the pursuit of life balance, when not engaged in coding or studying, I enjoy maintaining an active lifestyle at the gym and hanging out with friends. You'll find my humble <mark className={`highlight-${mode}`}>portfolio</mark> down below.
        </p>
      </div>

      <p className="scroll-hint">{"< --- >"}</p>

      <div className="portfolio-scroll">
          <Portfolio
            name="STAFF MANAGER"
            description="Simple staff/employee management software where I learned more about React (mainly states) and utilized some Bootstrap components."
            url="https://gitlab.com/samuel-m99/staff-manager"
            mode={mode}
          />

          <Portfolio
            name="PERSONAL WEBSITE"
            description="The webpage that you are currently viewing. I've made it using React, HTML, and CSS. I've incorporated some of my personal design preferences into its layout and appearance."
            url="https://gitlab.com/samuel-m99/personal-website"
            mode={mode}
          />

          <Portfolio
            name="TBA"
            description=""
            url=""
            mode={mode}
          />

          <Portfolio
            name="TBA"
            description=""
            url=""
            mode={mode}
          />
      </div>
    </div>
  );
}

export default App;
